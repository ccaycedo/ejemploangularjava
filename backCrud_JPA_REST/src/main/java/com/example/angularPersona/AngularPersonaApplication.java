package com.example.angularPersona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularPersonaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularPersonaApplication.class, args);
	}

}
