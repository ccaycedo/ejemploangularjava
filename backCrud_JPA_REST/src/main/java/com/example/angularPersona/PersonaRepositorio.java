package com.example.angularPersona;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

public interface PersonaRepositorio extends CrudRepository<Persona, Integer> {
	List<Persona> findAll();	
	void delete (Persona p);
}
