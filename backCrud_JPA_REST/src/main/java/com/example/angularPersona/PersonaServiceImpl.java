package com.example.angularPersona;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImpl implements PersonaService{

	@Autowired
	private PersonaRepositorio repo;
	
	@Override
	public List<Persona> listar() {
		
		return repo.findAll();
	}

	@Override
	public Persona listarId(int id) {
		
		return repo.findById(id).get();
	}

	@Override
	public Persona add(Persona p) {		
		return repo.save(p);
	}

	@Override
	public Persona editar(Persona p) {
		
		return repo.save(p);
	}

	@Override
	public Persona delete(int id) {
		Persona p=repo.findById(id).get();
		if(p != null) {
			repo.delete(p);
		}
		return p;
	}

}
